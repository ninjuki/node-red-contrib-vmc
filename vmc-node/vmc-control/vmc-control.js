var _ = require('underscore');
var VmcController = require('../VmcController.js');

module.exports = function(RED) {
   "use strict";

   // The main node definition - most things happen in here
   function vmcControl(config) {

      // Create a RED node
      RED.nodes.createNode(this, config);

      // Store local copies of the node configuration (as defined in the .html)
      var node = this;
    
      this.topic = config.topic;

      // If we are to return an array
      this.returnArray = false;

      // Load information from the devices list
      this.loadDeviceData = function() {
         var deviceList = [];
         return deviceList;
      }

      this.buferDataComplete = function() {

            return !!VmcController.Datas.TemperatureInterieur
            && !!VmcController.Datas.TemperatureExterieur
            && !!VmcController.Datas.TemperatureConsigne

            ;

      }

      this.ByPass = function(inMsg){

        if(!this.buferDataComplete()) return null;

        var previousState = VmcController.States.ByPass;

        if(VmcController.Datas.TemperatureExterieur < VmcController.Datas.TemperatureInterieur
            && VmcController.Datas.TemperatureInterieur > VmcController.Datas.TemperatureConsigne){
            
            VmcController.States.ByPass = 1;
            node.status({ fill: "blue", shape: "dot", text: "By-Pass" });
        }else{
            VmcController.States.ByPass = 0;
            node.status({ fill: "red", shape: "dot", text: "no - By-Pass" });
        }

        if(VmcController.States.ByPass === previousState) return null;

        var msg = _.clone(inMsg);
        msg.payload = VmcController.States.ByPass;

        return msg;

      }

        this.OnOff = function(inMsg){

            var previousState = VmcController.States.OnOff;

            if(VmcController.States.OnOff !== VmcController.Requests.OnOff)
                VmcController.States.OnOff = VmcController.Requests.OnOff;

            if(VmcController.States.OnOff === previousState) return null;

            var msg = _.clone(inMsg);
            msg.payload = VmcController.States.OnOff;

            return msg;
        }

        this.Maintenance = function(inMsg){

            var previousState = VmcController.States.Maintenance;

            if(VmcController.States.Maintenance !== VmcController.Requests.Maintenance)
                VmcController.States.Maintenance = VmcController.Requests.Maintenance;

            if(VmcController.States.Maintenance === previousState) return null;

            if(VmcController.States.Maintenance === 1){
                //INFO : Met sur Off la VMC.
                VmcController.Requests.OnOff = 1;
            }

            var msg = _.clone(inMsg);
            msg.payload = VmcController.States.Maintenance;

            return msg;
        }

        this.Vitesse = function(inMsg){

            var previousState = VmcController.States.Vitesse;

            if(VmcController.States.Vitesse !== VmcController.Requests.Vitesse)
                VmcController.States.Vitesse = VmcController.Requests.Vitesse;

            if(VmcController.States.Vitesse === previousState) return [null, null];

            var msg = _.clone(inMsg);
            msg.payload = VmcController.States.Vitesse;

            return [msg, _.clone(msg)];
        }

      this.read = function(inMsg) {
         var deviceList = this.loadDeviceData();

        switch(inMsg.topic) {
            case VmcController.Topics.TempInterieur:
                VmcController.Datas.TemperatureInterieur = inMsg.payload;
                break;
            case VmcController.Topics.TempExterieur:
                VmcController.Datas.TemperatureExterieur = inMsg.payload;
                break;
            case VmcController.Topics.TempConsigne:
                VmcController.Datas.TemperatureConsigne = inMsg.payload;
                break;
            case VmcController.Topics.OnOff:

                if(VmcController.States.Maintenance === 1){
                    //INFO : Blocage en cas de Maintenance
                    node.status({ fill: "red", shape: "dot", text: "Maintenance on" });
                    return null;
                }

                VmcController.Requests.OnOff = inMsg.payload;
                break;
            case VmcController.Topics.Maintenance:
                VmcController.Requests.Maintenance = inMsg.payload;
                break;                
            case VmcController.Topics.Vitesse:
                
                if(VmcController.States.Maintenance === 1){
                    //INFO : Blocage en cas de Maintenance
                    node.status({ fill: "red", shape: "dot", text: "Maintenance on" });
                    return null;
                }

                VmcController.Requests.Vitesse = inMsg.payload;
                break;                                
            default:
                return null;
        }

        var maintenance = this.Maintenance(inMsg);
        var vitesse = this.Vitesse(inMsg);

            var result = [
                this.ByPass(inMsg), 
                vitesse[0], 
                vitesse[1], 
                this.OnOff(inMsg), 
                maintenance];
                
            return result;
      };

      this.on('input', function (msg) {
         var arr = this.read(msg);
         
         if (arr) {
            node.send(arr);
         }else{
             node.status({});
         }
      });

    RED.httpAdmin.get('/vmc/', function (req, res) {
        res.json({Datas: VmcController.Datas, States: VmcController.States});
    });

      this.on("close", function() {
            node.status({});
        });

   }

   // Register the node by name.
   RED.nodes.registerType("vmc-control", vmcControl);
}


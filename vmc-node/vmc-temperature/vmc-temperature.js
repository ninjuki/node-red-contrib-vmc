var VmcController = require('../VmcController.js');
var upcast = require('upcast');

module.exports = function(RED) {

   function vmcTemperature(config) {

      RED.nodes.createNode(this, config);
      var node = this;

      this.name = config.name;
      this.source = config.source;

    this.units = config.units || "s";
    this.duration = config.duration || 30 * 1000 * 60;

    var tout = null;

    this.ConvertPayload = function(inMsg) {

        if(upcast.is(inMsg.payload, 'string')){
            inMsg.payload = upcast.to(inMsg.payload, 'number');
        }

    }

    this.Convert = function(value) {

        if(upcast.is(value, 'string')){
            return upcast.to(value, 'number');
        }
        return value;
    }

      this.read = function(inMsg) {

        inMsg.topic = this.source;        

        switch(this.source) {
            case VmcController.Topics.TempInterieur:
                this.ConvertPayload(inMsg);
                node.status({ fill: "blue", shape: "ring", text: inMsg.payload });
                break;
            case VmcController.Topics.TempExterieur:
                this.ConvertPayload(inMsg);
                node.status({ fill: "yellow", shape: "ring", text: inMsg.payload });
                break;
            case VmcController.Topics.TempConsigne:
                this.ConvertPayload(inMsg);
                node.status({ fill: "grey", shape: "ring", text: inMsg.payload });
                break;
            case VmcController.Topics.Maintenance:
                this.ConvertPayload(inMsg);
                node.status({ fill: inMsg.payload == 1 ? "yellow" : "green", shape: "dot", text: inMsg.payload });
                break;
            case VmcController.Topics.OnOff:

                if(VmcController.States.Maintenance === 1){
                    //INFO : Blocage en cas de Maintenance
                    node.status({ fill: "red", shape: "dot", text: "Maintenance on" });
                    return null;
                }

                this.ConvertPayload(inMsg);
                node.status({ fill: inMsg.payload == 0 ? "green" : "red", shape: "dot", text: inMsg.payload });
                break;
            case VmcController.Topics.Vitesse:
                
                if(VmcController.States.Maintenance === 1){
                    //INFO : Blocage en cas de Maintenance
                    node.status({ fill: "red", shape: "dot", text: "Maintenance on" });
                    return null;
                }

                if(VmcController.States.Vitesse === 0){
                    //INFO : délais dans le payload.
                    var duration = this.Convert(inMsg.payload || this.duration);
                    if (duration <= 0) { duration = this.duration; }

                    //INFO : Toujours action de mettre à grande vitesse.
                    inMsg.payload = 1;
                    node.status({ fill: "blue", shape: "dot", text: "Grande (" + duration + " " + this.units + ")"});

                    var durationms = this.duration;

                    if (this.units == "s") { durationms = duration * 1000; }
                    if (this.units == "min") { durationms = duration * 1000 * 60; }
                    if (this.units == "hr") { durationms = duration * 1000 *60 * 60; }
                    
                    tout = setTimeout(function() {
                        var msg2 = RED.util.cloneMessage(inMsg);
                        msg2.payload = 0;   //INFO : Remet en petite vitesse.
                        node.send(msg2);
                        tout = null;
                        node.status({ fill: "green", shape: "dot", text: "Petite" });
                    },durationms);
                }else{
                    if (tout) {
                        clearTimeout(tout);
                    }                    
                    inMsg.payload = 0;   //INFO : Remet en petite vitesse.
                    tout = null;
                    node.status({ fill: "green", shape: "dot", text: "Petite" });
                }
                break;
            default:
                node.status({ fill: "red", shape: "dot", text: this.source });
                return null;
        }

         return inMsg;
      };

      this.on('input', function (msg) {

         var r = this.read(msg);

         if (r) {
            node.send(r);
         }
      });

      this.on("close", function() {
            if (tout) {
                clearTimeout(tout);
            }
            node.status({});
        });

   }

   RED.nodes.registerType("vmc-temperature", vmcTemperature);
}


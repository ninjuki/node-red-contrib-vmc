'use strict';

var datas = {
  TemperatureConsigne: null,
  TemperatureExterieur: null,
  TemperatureInterieur: null,
};
var states = {
  ByPass: 0,
  Maintenance: 0,
  OnOff: 1,
  Vitesse: 0,
};
var requests = {};

var addX = function(value) {
  return value + x;
};

module.exports.addX = addX;
module.exports.Datas = datas;
module.exports.Requests = requests;
module.exports.States = states;

module.exports.Topics = {
    TempInterieur:   'vmcTempInterieur', 
    TempExterieur:  'vmcTempExterieur', 
    TempConsigne:  'vmcTempConsigne', 
    Maintenance: 'vmcMaintenance', 
    OnOff:'vmcOnOff', 
    Vitesse: 'vmcVitesse', 
};



